﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Otus_MultithreadCalculation
{
    class Program
    {
        private static Object locker = new Object();
        private static int _threadSummarizeResult = default;
        static void Main(string[] args)
        {
            List<int> list2calculate100k, list2calculate1m, list2calculate10m;
            var TimeObserver = Stopwatch.StartNew(); // запуск замера времения генерации массива
            list2calculate100k = RandArrayGenerator(100000);  // генерируем массив из 100k int элементов
            list2calculate1m   = RandArrayGenerator(1000000); // генерируем массив из 1 млн int элементов
            list2calculate10m  = RandArrayGenerator(10000000);// генерируем массив из 10 млн int элементов
            TimeObserver.Stop();

            getSumAsUsual(list2calculate100k, true);
            getSumAsParrallel(list2calculate100k);
            getSumAsPLINQ(list2calculate100k);

            getSumAsUsual(list2calculate1m, true);
            getSumAsParrallel(list2calculate1m);
            getSumAsPLINQ(list2calculate1m);

            getSumAsUsual(list2calculate10m, true);
            getSumAsParrallel(list2calculate10m);
            getSumAsPLINQ(list2calculate10m);


            getSumAsPLINQ(list2calculate100k);

            Console.ReadLine();
        }
        // Возвращает массив для дальнейшего использования
        private static List<int> RandArrayGenerator(int desirableArraySize)
        {
            List<int> generatedArray = new List<int>();
            Random valueRandomizer = new Random();
            for (int i = 0; i < desirableArraySize; i++)
            {
                generatedArray.Add(valueRandomizer.Next(100)); // Рандомно берём числа от 1 до 100 которые помещаем в массив
            }
            return generatedArray;
        }
        // Стандартный метод сложения чисел в массиве
        private static int getSumAsUsual(List<int> list2calculate, bool timeEstimationNeeded = false)
        {
            int result = default;
            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < list2calculate.Count; i++)
            {
                result += list2calculate[i];
            }
            sw.Stop();
            if (timeEstimationNeeded == true)
                Console.WriteLine($"Usual summarize has returned:{result} for the {list2calculate.Count} elements" +
                                  $" and has taken {sw.Elapsed.TotalMilliseconds} Mls");
            return result;
        }
        // Метод обёртка над обычным методом расчёта суммы для соответствия сигнатуре делегата ParameterizedThreadStart
        private static void getSumAsUsualDelegate(Object state)
        {
            lock (locker)
            {
                List<int> intermediateList = (List<int>)state;
                _threadSummarizeResult += getSumAsUsual(intermediateList, false);
            }
        }

        private static int getSumAsParrallel(List<int> list2calculate)
        {
            int result = default;
            List<List<int>> DataPartsToCalculate = new List<List<int>>();
            List<Thread> listOfThreads = new List<Thread>();
            int maxNumOfThreads = System.Environment.ProcessorCount; // получаем кол-во процессоров => кол-во потоков
            int iterationStep = list2calculate.Count / maxNumOfThreads; // размер блока цифр который будем обрабатывать в индивидуальном потоке
            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < maxNumOfThreads; i++)
            {
                List<int> curPart = list2calculate.Skip(iterationStep * i) // пропускаем часть которую уже взяли
                                          .Take(iterationStep) // берём очередную часть равную iterationStep
                                          .ToList();
                DataPartsToCalculate.Add(curPart); // Часть данных полученных из  LINQ запроса сохраняем в лист
                // Заполняем лист экзеплярами Thread для последующего вызова
                // В качестве аргумента ParameterizedThreadStart нужен метод для обработки подходящиц по сигнатуре делегату
                // Внутри getSumAsUsualDelegate вызывается getSumAsUsual 
                listOfThreads.Add(new Thread(new ParameterizedThreadStart(getSumAsUsualDelegate)));
            }
            // проверяем что кол-во потоков готовых к запуску = кол-ву элементов к обработке
            if (DataPartsToCalculate.Count == listOfThreads.Count)
            {
                for (int i = 0; i < DataPartsToCalculate.Count; i++)
                {
                    listOfThreads[i].Start(DataPartsToCalculate[i]);// запускаем потоки и передаём им данные для обработки
                }
                while (listOfThreads.Any(thread => thread.IsAlive)) // Крутим цикл тут до тех пор пока есть хоть один живой сабтред
                {
                }
            }
            else
                throw new Exception("The numbers of thread aren't equals to numbers of dataParts");
            sw.Stop();
            Console.WriteLine($"Parrallel summarize has returned:{_threadSummarizeResult} for the {list2calculate.Count}" +
                              $" and has taken {sw.Elapsed.TotalMilliseconds} Mls");
            return result;
        }

        private static void getSumAsPLINQ(List<int> list2calculate)
        {
            Stopwatch sw = Stopwatch.StartNew();
            int plinqResult = default;
            plinqResult = list2calculate.AsParallel().Sum();
            sw.Stop();
            Console.WriteLine($"PLINQ summarize has returned:{plinqResult} for the {list2calculate.Count}" +
                              $" and has taken {sw.Elapsed.TotalMilliseconds} Mls");
        }
    }
}
